#!/bin/bash
usage()
{
   echo "$(basename $0) <hostname>"
   echo 
   echo "Create a CDashboard on <hostname>"
}

[ $# -eq 1 ] || { usage; exit 1; }
command -v oc || { echo "The oc command must be available"; exit 2; }

hostname=$1
(echo "${hostname}" | grep -q '\.') || hostname=${hostname}.apps.math.cnrs.fr

project=$(echo ${hostname} | cut -d. -f1)
service_name="cdash"

echo "Project name: ${project}"
echo "Dashboard URL: https://${service_name}-${hostname}"

set -e
oc new-project ${project} --display-name="CDash" --description="CDashboard"

oc create -f ./yaml/cdash-claim1-persistentvolumeclaim.yaml
oc create -f ./yaml/cdash-claim2-persistentvolumeclaim.yaml
oc create -f ./yaml/mysqldata-persistentvolumeclaim.yaml
oc create -f ./yaml/storage-persistentvolumeclaim.yaml


oc new-app --name database --template=openshift/mysql-persistent -p MYSQL_USER=cdash -p MYSQL_DATABASE=cdash 
pw=$(oc get secret mysql --template='{{index .data "database-password"}}' | base64 -d)
# Note : création d'un volume persistent mysql et montage dans /var/lib/mysql/data


sed "s|secretpass|${pw}|g" env-file > .env


# oc volume dc/mysql --add --name mysqldata --mount-path=/var/lib/mysql
# inutile

#oc new-app https://gricad-gitlab.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift.git -e CDASH_ROOT_ADMIN_PASS=SECRET -e CDASH_CONFIG='$CDASH_DB_LOGIN = "cdash"; $CDASH_DB_HOST = "mysql"; $CDASH_DB_PASS = "'${pw}'"; $CDASH_ASSET_URL = "https://'${hostname}'"; $CDASH_BASE_URL = "https://'${hostname}'";'
#oc new-app gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/cdash:v3.1.0 --name ${service_name} -e CDASH_ROOT_ADMIN_PASS='toto' -e CDASH_CONFIG='$CDASH_DB_LOGIN = "cdash"; $CDASH_DB_HOST = "mysql"; $CDASH_DB_PASS = "'${pw}'"; $CDASH_ASSET_URL = "https://'${hostname}'"; $CDASH_BASE_URL = "https://'${hostname}'";'


oc new-app gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/mycdash:v3.2.0 --name cdash --env-file=./.env


oc new-app gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/mycdash-worker:v3.2.0 --name cdash-worker --env-file=./.env

#oc create deploymentconfig my-cdash --image=gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/cdash:v3.1.0
#oc set env dc/my-dash CDASH_ROOT_ADMIN_PASS="toto"
#oc set env dc/my-dash CDASH_CONFIG='$CDASH_DB_LOGIN = "cdash"; $CDASH_DB_HOST = "mysql"; $CDASH_DB_PASS = "'${pw}'"; $CDASH_ASSET_URL = "https://'${hostname}'"; $CDASH_BASE_URL = "https://'${hostname}'";'
#oc create route edge --service cdash-openshift --insecure-policy=Redirect --hostname ${hostname}

oc create route edge --port 8080 --service ${service_name}



echo
echo "All done. Now run"
echo
echo "    oc status"
echo
echo "and wait until deployment/cdash-openshift is running."
echo
echo "    oc wait --for=condition=Ready deployment/cdash-openshift --timeout=200s"
echo
echo "Finally point your browser to https://${hostname} to do the initial installation."
echo

