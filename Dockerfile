FROM kitware/cdash:v3.1.0

RUN apt update && apt install -y telnet
RUN apt clean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/
RUN npx browserslist@latest --update-db
RUN chmod -R a+w /home/kitware/cdash
RUN chmod -R a+w /etc/apache2
# Change port numbers to 8080 instead 80 since
# we don't have the privileges for the latter
RUN sed -i 's/^Listen [0-9][0-9]*/Listen 8080/g' /etc/apache2/ports.conf
RUN sed -i 's/^<VirtualHost \*:[0-9][0-9]*>/<VirtualHost \*:8080>/g' \
    /etc/apache2/sites-enabled/*.conf 
EXPOSE 8080
